namespace Base2art.MessageQueue.Management
{
    using System;
    using System.Threading.Tasks;

    public class SimpleMessageQueue : IMessageQueue
    {
        private readonly IMessageQueueStore store;

        public SimpleMessageQueue(IMessageQueueStore store)
            => this.store = store ?? throw new ArgumentNullException(nameof(store));

        public Task Push<T>(T valueToPush) where T : ITypedIdMessage
            => valueToPush == null
                   ? Task.CompletedTask
                   : this.store.Enqueue(valueToPush.TypeId, valueToPush);

        public Task PushMessage(Guid messageType, object valueToPush)
            => valueToPush == null
                   ? Task.CompletedTask
                   : this.store.Enqueue(messageType, valueToPush);
    }
}