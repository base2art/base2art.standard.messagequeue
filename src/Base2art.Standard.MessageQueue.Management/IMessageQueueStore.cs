namespace Base2art.MessageQueue.Management
{
    using System;
    using System.Threading.Tasks;

    public interface IMessageQueueStore
    {
        Task<IMessage> Dequeue();

        Task Requeue(Guid currentId, Guid currentValueType, object currentValue, int currentLevel);

        Task Enqueue(Guid type, object valueToPush);

        Task<int> Count();

        Task Complete(Guid currentId);
    }
}