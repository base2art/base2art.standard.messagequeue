namespace Base2art.MessageQueue.Management.Internals
{
    using System;

    internal class TypeMap
    {
        public Guid MessageType { get; set; }

        public IMessageHandler Handler { get; set; }
//        public Type Type { get; set; }
    }
}