namespace Base2art.MessageQueue.Management.Internals
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    internal static class MessageHandlers
    {
        private static readonly Dictionary<Guid, Task<IReadOnlyList<TypeMap>>> typeMapping = new Dictionary<Guid, Task<IReadOnlyList<TypeMap>>>();

        public static async Task<IMessageHandler[]> HandlersFor(this IMessageHandler[] messageHandlers, Guid messageType)
        {
            var map = await messageHandlers.GetTypeMap();
            var typeMaps = map.Where(x => x.MessageType == messageType)
                              .Select(x => x.Handler)
                              .ToArray();
            return typeMaps;
        }

        private static Task<IReadOnlyList<TypeMap>> GetTypeMap(this IMessageHandler[] handlers)
        {
            var key = string.Join("::", handlers.Select(x => x.GetType().FullName).OrderBy(x => x)).HashAsGuid();
            if (!typeMapping.ContainsKey(key))
            {
                return typeMapping[key] = handlers.GetTypeMapInternal();
            }

            return typeMapping[key];
        }

        private static async Task<IReadOnlyList<TypeMap>> GetTypeMapInternal(this IMessageHandler[] handlers)
        {
            var map = new List<TypeMap>();
            foreach (var handler in handlers)
            {
                var messageType = await handler.GetSupportedMessageType();
//                foreach (var messageType in await handler.GetSupportedMessageType())
                {
                    map.Add(new TypeMap
                            {
                                MessageType = messageType,
                                Handler = handler
//                                Type = messageType
                            });
                }
            }

            return map;
        }
    }
}