namespace Base2art.MessageQueue.Management
{
    using System;
    using System.Threading.Tasks;
    using Internals;

    public class MessageHandlerService
    {
        private readonly IMessageHandler[] handlers;
        private readonly IMessageQueueStore store;

        public MessageHandlerService(IMessageQueueStore store, IMessageHandler[] handlers)
        {
            this.store = store ?? throw new ArgumentNullException(nameof(store));
            this.handlers = handlers ?? throw new ArgumentNullException(nameof(handlers));
        }

        public async Task<bool> ProcessItem()
        {
            var current = await this.store.Dequeue();
            if (current == null)
            {
                return false;
            }

            var currentType = current.Type;
            var typeMaps = await this.handlers.HandlersFor(currentType);

            if (typeMaps.Length <= 0)
            {
                await this.store.Requeue(current.Id, currentType, current.Value, current.Level + 1);
                return false;
            }

            try
            {
                var handler = typeMaps[0];

                await handler.Handle(current);
                await this.store.Complete(current.Id);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.GetType().FullName);
                Console.WriteLine(e.StackTrace);

                await this.store.Requeue(current.Id, currentType, current.Value, current.Level + 1);

                return false;
            }
        }
    }
}

//
//        private async Task<bool> PopHandle(IReadOnlyList<TypeMap> map)
//        {
//            var current = await this.Dequeue();
//            if (current == null)
//            {
//                return false;
//            }
//
//            var currentType = current.Value.GetType();
//            var typeMaps = map.Where(x => x.MessageType == currentType)
//                              .ToArray();
//            if (typeMaps.Length <= 0)
//            {
//                await this.Enqueue(new Message(current.Id, current, current.Level));
//            }
//
//            try
//            {
//                await Task.WhenAll(typeMaps.Select(x => x.Handler.Handle(current.Value)));
//                return true;
//            }
//            catch (Exception e)
//            {
//                Console.WriteLine(e.Message);
//                Console.WriteLine(e.GetType().FullName);
//                Console.WriteLine(e.StackTrace);
//
//                await this.Enqueue(new Message(current.Id, current, current.Level + 1));
//            }
//
//            return false;
//        }
//
//        private class TypeMap
//        {
//            public Type MessageType { get; set; }
//            public IMessageHandler Handler { get; set; }
//        }
//
//        protected class Message
//        {
//            public Message(object value, int level)
//            {
//                this.Id = Guid.NewGuid();
//                this.Value = value;
//                this.Level = level;
//            }
//
//            public Message(Guid id, object value, int level)
//            {
//                this.Id = id;
//                this.Value = value;
//                this.Level = level;
//            }
//
//            public Guid Id { get; }
//            public object Value { get; }
//            public int Level { get; }
//        }