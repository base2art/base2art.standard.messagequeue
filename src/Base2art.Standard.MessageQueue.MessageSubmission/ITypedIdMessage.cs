﻿namespace Base2art.MessageQueue
{
    using System;

    public interface ITypedIdMessage
    {
        Guid TypeId { get; }
    }
}