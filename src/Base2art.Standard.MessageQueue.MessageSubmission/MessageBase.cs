namespace Base2art.MessageQueue
{
    using System;

    public class MessageBase : ITypedIdMessage
    {
        public Guid TypeId => this.GetType().FullName.HashAsGuid();
    }
}