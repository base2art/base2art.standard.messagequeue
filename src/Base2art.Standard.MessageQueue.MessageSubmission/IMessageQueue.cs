﻿namespace Base2art.MessageQueue
{
    using System;
    using System.Threading.Tasks;

    public interface IMessageQueue
    {
        Task Push<T>(T valueToPush)
            where T : ITypedIdMessage;

        Task PushMessage(Guid messageType, object valueToPush);
    }
}