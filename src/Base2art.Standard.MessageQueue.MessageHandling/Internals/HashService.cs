﻿namespace Base2art.MessageQueue.Internals
{
    using System;
    using System.Globalization;
    using System.Security.Cryptography;
    using System.Text;

    internal static class HashService
    {
        public static Guid HashAsGuid(this string value)
        {
            return Create(MD5.Create, value, x => new Md5HashResult(x)).AsGuid();
        }

        private static TResult Create<T, TResult>(string value, Func<byte[], TResult> creator)
            where T : HashAlgorithm, new()
            where TResult : HashResult
        {
            return Create(() => new T(), value, creator);
        }

        private static TResult Create<TResult>(Func<HashAlgorithm> algorithmProvider, string value, Func<byte[], TResult> creator)
            where TResult : HashResult
        {
            var inputBytes = Encoding.ASCII.GetBytes(value ?? string.Empty);
            using (var hashAlgorithm = algorithmProvider())
            {
                return creator(hashAlgorithm.ComputeHash(inputBytes));
            }
        }

        private class HashResult : IHashResult
        {
            public HashResult(byte[] hashedBytes) => this.HashedBytes = hashedBytes;

            protected byte[] HashedBytes { get; }

            public byte[] AsByteArray() => this.HashedBytes;

            public string AsString()
            {
                var hash = this.HashedBytes;

                // step 2, convert byte array to hex string
                var sb = new StringBuilder();
                for (var i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2", CultureInfo.InvariantCulture));
                }

                return sb.ToString();
            }
        }

        private interface IHashResult
        {
            string AsString();

            byte[] AsByteArray();
        }

        private interface I16ByteHashResult : IHashResult
        {
            Guid AsGuid();
        }

        private class Md5HashResult : HashResult, I16ByteHashResult
        {
            public Md5HashResult(byte[] hashedBytes) : base(hashedBytes)
            {
            }

            public Guid AsGuid()
            {
                var hashedBytes = this.HashedBytes;
                var bytes = new byte[16];
                bytes[0] = hashedBytes[3];
                bytes[1] = hashedBytes[2];
                bytes[2] = hashedBytes[1];
                bytes[3] = hashedBytes[0];
                bytes[4] = hashedBytes[5];
                bytes[5] = hashedBytes[4];
                bytes[6] = hashedBytes[7];
                bytes[7] = hashedBytes[6];
                bytes[8] = hashedBytes[8];
                bytes[9] = hashedBytes[9];
                bytes[10] = hashedBytes[10];
                bytes[11] = hashedBytes[11];
                bytes[12] = hashedBytes[12];
                bytes[13] = hashedBytes[13];
                bytes[14] = hashedBytes[14];
                bytes[15] = hashedBytes[15];
                return new Guid(bytes);
            }
        }
    }
}