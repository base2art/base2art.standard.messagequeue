namespace Base2art.MessageQueue
{
    using System;
    using System.Threading.Tasks;

    public interface IMessageHandler
    {
        Task<Guid> GetSupportedMessageType();

        Task Handle(IMessage message);
    }
}