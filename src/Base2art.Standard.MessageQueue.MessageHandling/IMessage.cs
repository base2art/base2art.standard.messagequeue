namespace Base2art.MessageQueue
{
    using System;

    public interface IMessage
    {
        Guid Id { get; }
        object Value { get; }
        int Level { get; }
        Guid Type { get; }
    }
}