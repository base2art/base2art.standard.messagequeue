namespace Base2art.MessageQueue
{
    using System;
    using System.Threading.Tasks;
    using Internals;
    using Internals.Serialization;

    public abstract class MessageHandlerBase<T> : IMessageHandler
        where T : class
    {
        private readonly CamelCasingSimpleJsonSerializer serializer;

        protected MessageHandlerBase() => this.serializer = new CamelCasingSimpleJsonSerializer();

        Task<Guid> IMessageHandler.GetSupportedMessageType()
            => Task.FromResult(typeof(T).FullName.HashAsGuid());

        Task IMessageHandler.Handle(IMessage message)
        {
            var messageValue = message.Value;
            if (messageValue is T variable)
            {
                return this.HandleMessage(variable);
            }

            var val = this.serializer.Serialize(messageValue);

            var objTyped = this.serializer.Deserialize<T>(val);
            return this.HandleMessage(objTyped);
        }

        protected abstract Task HandleMessage(T message);
    }
}