namespace Base2art.MessageQueue.Storage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class InMemoryMessageQueueStore : MessageQueueStoreBase<Dictionary<Guid, SimpleMessage>, SimpleMessage>
    {
        public InMemoryMessageQueueStore()
            : base(new Dictionary<Guid, SimpleMessage>(),
                   new Dictionary<Guid, SimpleMessage>(),
                   new Dictionary<Guid, SimpleMessage>())
        {
        }

        protected override Task<int> GetCount(Dictionary<Guid, SimpleMessage> channel)
            => Task.FromResult(channel.Count);

        protected override Task Add(Dictionary<Guid, SimpleMessage> channel, Guid id, SimpleMessage simpleMessage)
            => Task.Factory.StartNew(() => channel[id] = simpleMessage);

        protected override Task MoveTo(
            SimpleMessage first,
            Dictionary<Guid, SimpleMessage> oldChannel,
            Dictionary<Guid, SimpleMessage> newChannel,
            Guid messageId)
        {
            oldChannel.Remove(messageId);
            newChannel[messageId] = first;
            return Task.CompletedTask;
        }

        protected override SimpleMessage GetMessageFromFile(SimpleMessage first) => first;

        protected override SimpleMessage GetFirstFrom(Dictionary<Guid, SimpleMessage> channel) => channel.Values.FirstOrDefault();

        protected override Task RemoveFrom(Dictionary<Guid, SimpleMessage> channel, Guid messageId)
        {
            channel.Remove(messageId);
            return Task.CompletedTask;
        }
    }
}