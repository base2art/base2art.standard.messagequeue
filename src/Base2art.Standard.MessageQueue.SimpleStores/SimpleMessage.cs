namespace Base2art.MessageQueue.Storage
{
    using System;

    public class SimpleMessage
    {
        public Guid TypeId { get; set; }

        public Guid Id { get; set; }

        public object Value { get; set; }

        public int Level { get; set; }
    }
}