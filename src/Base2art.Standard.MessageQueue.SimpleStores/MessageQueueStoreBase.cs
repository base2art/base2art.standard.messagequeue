namespace Base2art.MessageQueue.Storage
{
    using System;
    using System.Threading.Tasks;
    using Management;

    public abstract class MessageQueueStoreBase<TChannel, TMessage> : IMessageQueueStore
        where TChannel : class
    {
        private readonly TChannel processingDirectory;
        private readonly TChannel queueDirectory;
        private readonly TChannel stuckDirectory;

        protected MessageQueueStoreBase(
            TChannel queueDirectory,
            TChannel processingDirectory,
            TChannel stuckDirectory)
        {
            this.queueDirectory = queueDirectory ?? throw new ArgumentNullException(nameof(queueDirectory));
            this.processingDirectory = processingDirectory ?? throw new ArgumentNullException(nameof(processingDirectory));
            this.stuckDirectory = stuckDirectory ?? throw new ArgumentNullException(nameof(stuckDirectory));
        }

        async Task<IMessage> IMessageQueueStore.Dequeue()
        {
            var oldChannel = this.Channel(State.New);
            var first = this.GetFirstFrom(oldChannel);
            if (first == null)
            {
                return null;
            }

            var obj = this.GetMessageFromFile(first);

            await this.MoveTo(first, oldChannel, this.Channel(State.Processing), obj.Id);

            var typeName = obj.TypeId;

            return new Message(obj.Id, obj.Value, obj.Level, typeName);
        }

        async Task IMessageQueueStore.Requeue(Guid currentId, Guid currentValueType, object currentValue, int currentLevel)
        {
            await this.Obsolete(currentId);
            var id = Guid.NewGuid();
            var channel = this.Channel(currentLevel > 2 ? State.Stuck : State.New);

            await this.Add(channel, id, new SimpleMessage
                                        {
                                            Id = id,
                                            Level = currentLevel,
                                            TypeId = currentValueType,
                                            Value = currentValue
                                        });
        }

        Task IMessageQueueStore.Enqueue(Guid type, object valueToPush)
        {
            var id = Guid.NewGuid();
            var channel = this.Channel(State.New);
            return this.Add(channel, id, new SimpleMessage
                                         {
                                             Id = id,
                                             Level = 0,
                                             TypeId = type,
                                             Value = valueToPush
                                         });
        }

        Task<int> IMessageQueueStore.Count()
        {
            var channel = this.Channel(State.New);
            return this.GetCount(channel);
        }

        public Task Complete(Guid currentId)
        {
            var channel = this.Channel(State.Processing);
            return this.RemoveFrom(channel, currentId);
        }

        protected abstract Task<int> GetCount(TChannel channel);

        protected abstract Task Add(TChannel channel, Guid id, SimpleMessage simpleMessage);

        protected abstract Task MoveTo(TMessage message, TChannel oldChannel, TChannel newChannel, Guid messageId);

        protected abstract SimpleMessage GetMessageFromFile(TMessage first);

        protected abstract TMessage GetFirstFrom(TChannel channel);

        protected abstract Task RemoveFrom(TChannel channel, Guid messageId);

        private Task Obsolete(Guid messageId)
        {
            var channel = this.Channel(State.Processing);
            return this.RemoveFrom(channel, messageId);
        }

        private TChannel Channel(State state)
        {
            switch (state)
            {
                case State.New:
                    return this.queueDirectory;
                case State.Processing:
                    return this.processingDirectory;
                case State.Stuck:
                    return this.stuckDirectory;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }

        private class Message : IMessage
        {
            public Message(Guid id, object value, int level, Guid type)
            {
                this.Id = id;
                this.Value = value;
                this.Level = level;
                this.Type = type;
            }

            public Guid Id { get; }
            public object Value { get; }
            public int Level { get; }
            public Guid Type { get; }
        }

        private enum State
        {
            New,
            Processing,
            Stuck
        }
    }
}