namespace Base2art.MessageQueue.Storage
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Serialization;

    public class FileSystemMessageQueueStore : MessageQueueStoreBase<DirectoryInfo, FileInfo>
    {
        private readonly CamelCasingSimpleJsonSerializer serializer;

        public FileSystemMessageQueueStore(string baseDir)
            : base(
                   Directory.CreateDirectory(Path.Combine(baseDir, "queue")),
                   Directory.CreateDirectory(Path.Combine(baseDir, "processing")),
                   Directory.CreateDirectory(Path.Combine(baseDir, "stuck")))
            => this.serializer = new CamelCasingSimpleJsonSerializer();

        protected override FileInfo GetFirstFrom(DirectoryInfo channel)
            => channel.EnumerateFiles("*.msg").FirstOrDefault();

        protected override Task MoveTo(FileInfo first, DirectoryInfo oldChannel, DirectoryInfo newChannel, Guid messageId)
        {
            first.MoveTo(this.PathOf(newChannel, messageId));
            return Task.CompletedTask;
        }

        protected override Task<int> GetCount(DirectoryInfo channel) => Task.FromResult(channel.GetFiles("*.msg").Length);

        protected override Task RemoveFrom(DirectoryInfo channel, Guid messageId)
        {
            var path = this.PathOf(channel, messageId);
            if (!File.Exists(path))
            {
                return Task.CompletedTask;
            }

            try
            {
                File.Delete(path);
            }
            catch (IOException)
            {
            }

            return Task.CompletedTask;
        }

        protected override Task Add(DirectoryInfo channel, Guid id, SimpleMessage message)
        {
            var path = this.PathOf(channel, id);
            var content = this.serializer.Serialize(message);

            File.WriteAllText(path, content);
            return Task.CompletedTask;
        }

        protected override SimpleMessage GetMessageFromFile(FileInfo x)
        {
            var content = File.ReadAllText(x.FullName);

            var obj = this.serializer.Deserialize<SimpleMessage>(content);
            return obj;
        }

        private string PathOf(DirectoryInfo state, Guid id)
            => Path.Combine(state.FullName, $"{id:N}.msg");
    }
}