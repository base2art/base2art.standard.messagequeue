namespace Base2art.Standard.MessageQueue.Features
{
    using System;
    using System.IO;

    public class FileSystemParameterProvider : IParameterProvider
    {
        public object[] Parameters => new object[]
                                      {
                                          Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N"))
                                      };
    }
}