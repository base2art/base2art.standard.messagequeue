namespace Base2art.Standard.MessageQueue.Features
{
    public interface IParameterProvider
    {
        object[] Parameters { get; }
    }
}