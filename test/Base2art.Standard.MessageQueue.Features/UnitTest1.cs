namespace Base2art.Standard.MessageQueue.Features
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.MessageQueue;
    using Base2art.MessageQueue.Management;
    using Base2art.MessageQueue.Storage;
    using FluentAssertions;
    using Xunit;

    public class MessageQueueFeature
    {
        public static readonly List<string> items = new List<string>();

//        [Theory]
//        [InlineData(typeof(InMemoryMessageQueueStore), null)]
//        [InlineData(typeof(FileSystemMessageQueueStore), typeof(FileSystemParameterProvider))]
//        public async void ShouldResolveRealWorld(Type ofStore, Type parameterProvider)
//        {
        [Fact]
        public async void ShouldResolveRealWorld()
        {
            items.Clear();

            IMessageQueueStore store = new InMemoryMessageQueueStore();
            var messageQueue = new SimpleMessageQueue(store);
            var messageReader = new MessageHandlerService(store, new IMessageHandler[]
                                                                 {
                                                                     new PullPushMessage_Phase1Handler(messageQueue),
                                                                     new PullPushMessage_Phase2Handler()
                                                                 });

            items.Count.Should().Be(0);
            (await store.Count()).Should().Be(0);
            await messageQueue.Push(new PullPushMessage_Phase1 {Name = "Sjy"});
            items.Count.Should().Be(0);
            (await store.Count()).Should().Be(1);
            await messageReader.ProcessItem();
            (await store.Count()).Should().Be(1);
            await messageReader.ProcessItem();
            items.Count.Should().Be(1);
            (await store.Count()).Should().Be(0);
        }

        [Theory]
        [InlineData(typeof(InMemoryMessageQueueStore), null)]
        [InlineData(typeof(FileSystemMessageQueueStore), typeof(FileSystemParameterProvider))]
        public async void ShouldHandleItemOnQueue(Type ofStore, Type parameterProvider)
        {
            items.Clear();
            var (messageQueue, messageReader, store) = this.MessageStore(ofStore, parameterProvider, new PersonMessageHandler());

            items.Count.Should().Be(0);
            (await store.Count()).Should().Be(0);
            await messageQueue.Push(new PersonMessage {Name = "Sjy"});
            items.Count.Should().Be(0);
            (await store.Count()).Should().Be(1);
            await messageReader.ProcessItem();
            items.Count.Should().Be(1);
            (await store.Count()).Should().Be(0);
        }

        [Theory]
        [InlineData(typeof(InMemoryMessageQueueStore), null)]
        [InlineData(typeof(FileSystemMessageQueueStore), typeof(FileSystemParameterProvider))]
        public async void ShouldHandleItemOnQueueNoReader(Type ofStore, Type parameterProvider)
        {
            items.Clear();
            var (messageQueue, messageReader, store) = this.MessageStore(ofStore, parameterProvider, new PersonMessageHandler());

            items.Count.Should().Be(0);
            (await store.Count()).Should().Be(0);

            await messageQueue.Push(new CompanyMessage {Name = "Sjy"});
            await messageQueue.Push(new PersonMessage {Name = "Sjy"});
            items.Count.Should().Be(0);
            (await store.Count()).Should().Be(2);

            for (int i = 0; i < 10; i++)
            {
                await messageReader.ProcessItem();
            }

            (await store.Count()).Should().Be(0);
            items.Count.Should().Be(1);
        }

        [Theory(Skip = "SjY Approved")]
        [InlineData(typeof(InMemoryMessageQueueStore), null)]
        [InlineData(typeof(FileSystemMessageQueueStore), typeof(FileSystemParameterProvider))]
        public async void ShouldHandleItemOfBaseTypeOnQueue(Type ofStore, Type parameterProvider)
        {
            items.Clear();
            var (messageQueue, messageReader, store) = this.MessageStore(ofStore, parameterProvider, new PersonInterfaceMessageHandler());

            items.Count.Should().Be(0);
            (await store.Count()).Should().Be(0);
            await messageQueue.Push(new PersonMessage {Name = "Sjy"});
            items.Count.Should().Be(0);
            (await store.Count()).Should().Be(1);
            await messageReader.ProcessItem();
            items.Count.Should().Be(1);
            (await store.Count()).Should().Be(0);
        }

        [Theory]
        [InlineData(typeof(InMemoryMessageQueueStore), null)]
        [InlineData(typeof(FileSystemMessageQueueStore), typeof(FileSystemParameterProvider))]
        public async void ShouldNotHandleItemListeningAndRequeue(Type ofStore, Type parameterProvider)
        {
            items.Clear();
            var (messageQueue, messageReader, store) = this.MessageStore(ofStore, parameterProvider, new PersonMessageHandler());

            items.Count.Should().Be(0);
            (await store.Count()).Should().Be(0);
            await messageQueue.Push(new CompanyMessage {Name = "Base2art"});
            items.Count.Should().Be(0);
            (await store.Count()).Should().Be(1);
            await messageReader.ProcessItem();
            items.Count.Should().Be(0);
            (await store.Count()).Should().Be(1);
        }

        private (IMessageQueue, MessageHandlerService, IMessageQueueStore) MessageStore(
            Type ofStore,
            Type parameterProvider,
            params IMessageHandler[] handlers)
        {
            var parms = parameterProvider == null
                            ? new object[0]
                            : ((IParameterProvider) Activator.CreateInstance(parameterProvider)).Parameters;

            var store = (IMessageQueueStore) Activator.CreateInstance(ofStore, parms.ToArray());
            IMessageQueue messageQueue = new SimpleMessageQueue(store);
            var messageReader = new MessageHandlerService(store, handlers);

            return (messageQueue, messageReader, store);
        }

        public class PullPushMessage_Phase1 : MessageBase
        {
            public string Name { get; set; }
        }

        public class PullPushMessage_Phase2 : MessageBase
        {
            public string Name { get; set; }
        }

        public class PullPushMessage_Phase1Handler : MessageHandlerBase<PullPushMessage_Phase1>
        {
            private readonly IMessageQueue queue;

            public PullPushMessage_Phase1Handler(IMessageQueue queue)
            {
                this.queue = queue;
            }

            protected override Task HandleMessage(PullPushMessage_Phase1 message) =>
                this.queue.Push(new PullPushMessage_Phase2 {Name = message.Name});
        }

        public class PullPushMessage_Phase2Handler : MessageHandlerBase<PullPushMessage_Phase2>
        {
            protected override Task HandleMessage(PullPushMessage_Phase2 message)
            {
                items.Add(message.Name);
                return Task.CompletedTask;
            }
        }

        private class CompanyMessage : MessageBase
        {
            public string Name { get; set; }
        }

        private class PersonMessage : MessageBase, IPersonMessage
        {
            public string Name { get; set; }
        }

        private class PersonMessageHandler : MessageHandlerBase<PersonMessage>
        {
            protected override Task HandleMessage(PersonMessage message)
            {
                items.Add(message.Name);
                return Task.CompletedTask;
            }
        }

        private class PersonInterfaceMessageHandler : MessageHandlerBase<IPersonMessage>
        {
            protected override Task HandleMessage(IPersonMessage message)
            {
                items.Add(message.Name);
                return Task.CompletedTask;
            }
        }

        private interface IPersonMessage
        {
            string Name { get; }
        }
    }
}